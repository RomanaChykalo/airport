package airport.controller;

import airport.model.entities.Client;
import airport.service.ClientService;
import airport.service.implementation.ClientServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Objects;
import java.util.Scanner;

import static airport.model.consts.ExceptionMessage.*;
import static airport.model.consts.controller.ClientControllerConsts.*;

public class ClientController {
    private static Logger logger = LogManager.getLogger(ClientController.class);
    private static Scanner input = new Scanner(System.in);
    private static ClientService clientService = new ClientServiceImpl();

    public static Integer createClientAndGetId(){
        logger.info(ENTER_CLIENT_NAME_MESSAGE);
        String name = input.nextLine();
        logger.info(ENTER_CLIENT_SURNAME_MESSAGE);
        String surname = input.nextLine();
        logger.info(ENTER_CLIENT_EMAIL_MESSAGE);
        String email = input.nextLine();
        logger.info(ENTER_CLIENT_PHONE_MESSAGE);
        String phone = input.nextLine();
        logger.info(ENTER_ID_NUMBER_MESSAGE);
        Integer id = input.nextInt();
        Client entity = new Client(id, name, surname, email, phone);
        int rowAmount = 0;
        try {
            rowAmount = clientService.create(entity);
        } catch (SQLException e) {
            logger.info(SQL_EXC_MESSAGE);
        }
        if (rowAmount > 0) {
            logger.info(GREETING_MESSAGE);
        } else {
            logger.error(ENTITY_WAS_NOT_CREATED_EXC);
            return createClientAndGetId();
        }
        return entity.getId();
    }

    public static Client findClientByIdAndEmail(){
        logger.info(ENTER_ID_MESSAGE);
        Integer id = input.nextInt();
        logger.info(ENTER_CLIENT_EMAIL_MESSAGE);
        input.nextLine();
        String email = input.nextLine();
        Client client = null;
        try {
            client = clientService.findByIdAndEmail(id, email);
            if (Objects.isNull(client)){
                logger.error(ENTITY_NOT_EXIST_EXC_MESSAGE);
                return findClientByIdAndEmail();
            }
        } catch (SQLException e) {
            logger.info(SQL_EXC_MESSAGE);
        }
        return client;
    }
}