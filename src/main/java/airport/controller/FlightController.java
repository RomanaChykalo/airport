package airport.controller;

import airport.service.FlightService;
import airport.service.implementation.FlightServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import static airport.model.consts.controller.FlightControllerConsts.*;
import static airport.model.consts.controller.FlightControllerConsts.DESTINATION_WORD;
import static airport.model.consts.entities.AircraftConsts.ID_WORD;
import static airport.model.consts.entities.FlightConsts.*;

public class FlightController {
    private static Logger logger = LogManager.getLogger(FlightController.class);
    private static Scanner input = new Scanner(System.in);
    private static FlightService flightService = new FlightServiceImpl();

    public static void printAllDestination() {
        List<String> cities = null;
        try {
            cities = flightService.findAllDestinations();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        cities.stream().forEach(System.out::println);
    }

    public static void printFlightsByOneCity() {
        logger.info(ENTER_CITY_MESSAGE);
        String city = input.nextLine();
        List<String> flights;
        flights = flightService.findByDestination(city);
        if (flights.size() == 0) {
            logger.info(NO_AVAILABLE_FLIGHTS_MESSAGE);
            printAllDestination();
            printFlightsByOneCity();
        } else {
            logger.info(String.format(FLIGHT_CONTROL_STRING_FORMAT, DESTINATION_WORD, ID_WORD, DEPARTURE_TIME_WORD,
                    ARRIVAL_TIME_WORD));
            for (String flight : flights) {
                logger.info(flight);
            }
        }


    }
}
