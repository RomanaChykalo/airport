package airport.controller;

import airport.exceptions.DuplicateEntityException;
import airport.service.OrderService;
import airport.service.implementation.OrderServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

import static airport.model.consts.ExceptionMessage.ENTITY_WAS_NOT_CREATED_EXC;
import static airport.model.consts.controller.OrderContrConsts.*;

public class OrderController {
    private static Logger logger = LogManager.getLogger(OrderController.class);
    private static OrderService orderService = new OrderServiceImpl();

    public static Integer createOrderAndReturnId(Integer clientId){
        int orderId = 0;
        try {
            orderId = orderService.create(clientId);
        } catch (SQLException e) {
            logger.error(CANNOT_CREATE_ORDER_FOR_NO_USER);
        }
        if (orderId > 0) {
            logger.info(REGISTRY_ORDER_GREETING_MESSAGE);
        } else {
            throw new DuplicateEntityException(ENTITY_WAS_NOT_CREATED_EXC);
        }
        return orderId;
    }
}
