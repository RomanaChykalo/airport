package airport.controller;

import airport.model.entities.Ticket;
import airport.service.TicketService;
import airport.service.implementation.TicketServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static airport.controller.FlightController.printAllDestination;
import static airport.controller.FlightController.printFlightsByOneCity;
import static airport.model.consts.ExceptionMessage.SQL_EXC_MESSAGE;
import static airport.model.consts.controller.TicketControllerConsts.*;

public class TicketController {
    private static Logger logger = LogManager.getLogger(TicketController.class);
    private static Scanner input = new Scanner(System.in);
    private static TicketService ticketService = new TicketServiceImpl();

    public static void printTicketsSortedByFlightId() {
        logger.info(ENTER_FLIGHT_ID_MESSAGE);
        Integer id = input.nextInt();
        List<Ticket> tickets;
        try {
            tickets = ticketService.findByFlightId(id);
            if (tickets.size() == 0) {
                logger.info(NO_AVAILABLE_TICKETS_ON_RAISE);
                printAllDestination();
                printFlightsByOneCity();
                printTicketsSortedByFlightId();
            } else {
                logger.info(COLUMN_NAMES_PATTERN);
                tickets.stream().forEach(System.out::println);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public static void updateTicket(Integer orderId, Integer seatPlaceId) {
        try {
            ticketService.update(orderId, seatPlaceId);
        } catch (SQLException e) {
            logger.error(NO_AVAILABLE_TICKETS_ON_RAISE);
        }
    }

    public static void printUserTickets(Integer userId) {
        try {
            logger.info(COLUMN_NAMES_PATTERN);
            List<Ticket> tickets = ticketService.findByClientId(userId);
            tickets.stream().forEach(System.out::println);
        } catch (SQLException e) {
            logger.info(e.getMessage());
        }
    }

    public static void printGeneralPrice(Integer userId) {
        Integer cost;
        try {
            cost = ticketService.getGeneralPrice(userId);
            if (cost == 0) {
                logger.info(NULLABLE_GENERAL_COST);
            } else {
                logger.info(GENERAL_TICKET_COST_MESSAGE + cost);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

    }

    public static Ticket findTicketById() {
        logger.info(ENTER_TICKET_ID_MESSAGE);
        Integer id = input.nextInt();
        Ticket ticket = null;
        try {
            ticket = ticketService.findById(id);
        } catch (SQLException e) {
            logger.error(SQL_EXC_MESSAGE);
        }
        if (!Objects.isNull(ticket)) {
            return ticket;
        } else {
            logger.info(ENTER_CORRECT_DATA_MESSAGE);
            return findTicketById();
        }
    }
}
