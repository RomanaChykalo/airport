package airport.dao;

import airport.model.entities.Client;

import java.sql.SQLException;

public interface ClientDao {
    int create(Client client) throws SQLException;

    Client findByIdAndEmail(Integer id, String email) throws SQLException;

}
