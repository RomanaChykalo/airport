package airport.dao;

import java.sql.SQLException;
import java.util.List;

public interface FlightDao {
    List<String> findAllDestinations() throws SQLException;
    List<String> findByDestination(String destination);
}
