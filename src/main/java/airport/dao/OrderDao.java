package airport.dao;

import java.sql.SQLException;
import java.util.List;

public interface OrderDao {
    Integer create(Integer clientId) throws SQLException;
}
