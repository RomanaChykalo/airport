package airport.dao.implementation;

import airport.dao.ClientDao;
import airport.model.entities.Client;
import airport.persistant.ConnectionManager;
import airport.persistant.Transformer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static airport.model.consts.dao.DaoConsts.*;

public class ClientDaoImpl implements ClientDao {
    private static Connection connection = ConnectionManager.getConnection();

    public int create(Client client) throws SQLException {
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(CREATE_CLIENT_SQL)) {
            preparedStatement.setString(1, client.getName());
            preparedStatement.setString(2, client.getSurname());
            preparedStatement.setString(3, client.getEmail());
            preparedStatement.setString(4, client.getPhone());
            preparedStatement.setInt(5, client.getId());
            return preparedStatement.executeUpdate();
        }
    }

    public Client findByIdAndEmail(Integer id, String email) throws SQLException {
        Client client = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_CLIENT_BY_ID_EMAIL_SQL)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    client = (Client) new Transformer(Client.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return client;
    }
}

