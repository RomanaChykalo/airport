package airport.dao.implementation;

import airport.dao.FlightDao;
import airport.persistant.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static airport.model.consts.dao.DaoConsts.*;

public class FlightDaoImpl implements FlightDao {
    private static Connection connection = ConnectionManager.getConnection();
    private static Logger logger = LogManager.getLogger(FlightDaoImpl.class);

    public List<String> findAllDestinations() throws SQLException {
        List<String> cities = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_DISTINCT_DESTINATION)) {
                while (resultSet.next()) {
                    String city = resultSet.getString(DESTINATION_WORD);
                    cities.add(city);
                }
            }
        }
        return cities;
    }

    public List<String> findByDestination(String destination) {
        List<String> flights = new ArrayList<>();
        String flightDetail = new String();
        try (PreparedStatement ps = connection.prepareStatement(FIND_AVAILABLE_FLIGHT_BY_DESTINATION_SQL)) {
            ps.setString(1, destination);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    Integer id = resultSet.getInt(ID_COLUMN);
                    String city = resultSet.getString(DESTINATION_COLUMN);
                    Timestamp departureTime = resultSet.getTimestamp(DEPARTURE_TIME_COLUMN);
                    Timestamp arrivalTime = resultSet.getTimestamp(ARRIVAL_TIME_COLUMN);
                    String concat = flightDetail.concat(city + EMPTY_SPACES_6).concat(String.valueOf(id +
                            EMPTY_SPACES_2)).concat(String.valueOf(departureTime + EMPTY_SPACES_4)
                            .concat(String.valueOf(arrivalTime + EMPTY_SPACES_4)));
                    flights.add(concat);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return flights;
    }
}
