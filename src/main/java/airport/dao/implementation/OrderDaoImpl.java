package airport.dao.implementation;

import airport.dao.OrderDao;
import airport.persistant.ConnectionManager;

import java.sql.*;

import static airport.model.consts.dao.DaoConsts.CREATE_ORDER_SQL;


public class OrderDaoImpl implements OrderDao {
    private static Connection connection = ConnectionManager.getConnection();

    public Integer create(Integer clientId) throws SQLException {
        Integer orderId = 0;
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(CREATE_ORDER_SQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setBoolean(2, false);
            preparedStatement.setInt(3, clientId);
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                orderId = generatedKeys.getInt(1);
            }
        }
        return orderId;
    }
}
