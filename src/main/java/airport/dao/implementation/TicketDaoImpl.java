package airport.dao.implementation;

import airport.dao.TicketDao;
import airport.model.entities.Ticket;
import airport.persistant.ConnectionManager;
import airport.persistant.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static airport.model.consts.dao.DaoConsts.*;

public class TicketDaoImpl implements TicketDao {
    private static Connection connection = ConnectionManager.getConnection();

    private List<Ticket> findByQuery(Integer property, String query) throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, property);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    tickets.add((Ticket) new Transformer(Ticket.class).fromResultSetToEntity(resultSet));
                }
                return tickets;
            }
        }
    }

    public List<Ticket> findByFlightId(Integer id) throws SQLException {
        return findByQuery(id, FIND_TICKET_BY_ID_SQL);
    }

    public List<Ticket> findByClientId(Integer clientId) throws SQLException {
        return findByQuery(clientId, FIND_TICKETS_BY_CLIENT_ID_SQL);
    }

    public Integer getGeneralPrice(Integer clientId) throws SQLException {
        Integer sum = 0;
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_GENERAL_PRICE_USER_TICKETS_SQL)) {
            preparedStatement.setInt(1, clientId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    sum = resultSet.getInt("sum");
                }
                return sum;
            }
        }
    }

    public void update(Integer orderId, Integer seatPlaceId) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_TICKET_AFTER_ORDER_SQL)) {
            ps.setInt(1, orderId);
            ps.setInt(2, seatPlaceId);
            ps.executeUpdate();
        }
    }

    public Ticket findById(Integer id) throws SQLException {
        Ticket entity = null;
        try (PreparedStatement ps = connection.prepareStatement(FIND_TICKET_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Ticket) new Transformer(Ticket.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }
}