package airport.model.consts;

public class ExceptionMessage {
    private ExceptionMessage() {
    }
    public static String SQL_EXC_MESSAGE = " Can't read/write information from/to the database";
    public static final String ENTITY_WAS_NOT_CREATED_EXC = " Entity was not created, entity with such id already" +
            " exists, please, try again, enter correct data\n";
    public static String ENTITY_NOT_EXIST_EXC_MESSAGE=" Client with such id and/or email doesn't exist, please try again";
}
