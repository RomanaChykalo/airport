package airport.model.consts;

public class GeneralViewConsts {
    private GeneralViewConsts() {
    }

    public static String LOGIN_OR_REGISTER_MESSAGE = "Would you like to register or you already have an account?" +
            " For login print l, registration r";
    public static String TYPE_YES_OR_NO_MESSAGE = "Type 'yes' to continue, 'no' for exit";
    public static String YES_WORD = "yes";
    public static String NO_WORD = "no";
    public static String CREATE_ORDER_MESSAGE = "1. Create order";
    public static String SHOW_ORDER_MESSAGE = "2. Show my orders";
    public static String QUIT_MESSAGE = "3. Quit program";
    public static String WELCOME_MESSAGE ="Welcome to Boston-airport cite!";
    public static String YOU_EXIT_MESSAGE = "You exit, see you next time!";
}
