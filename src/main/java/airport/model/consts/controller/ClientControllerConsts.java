package airport.model.consts.controller;

public class ClientControllerConsts {
    private ClientControllerConsts(){}
    public static final String ENTER_CLIENT_NAME_MESSAGE = "Enter your name: ";
    public static final String ENTER_CLIENT_SURNAME_MESSAGE = "Enter your surname: ";
    public static final String ENTER_CLIENT_PHONE_MESSAGE = "Enter your contact phone: ";
    public static final String ENTER_CLIENT_EMAIL_MESSAGE = "Enter your email: ";
    public static final String ENTER_ID_NUMBER_MESSAGE = "Enter your unique personal number-some number combination, which you will always remember" +
            " and use for login: ";
    public static final String GREETING_MESSAGE = "You have successfully registered on the site";
    public static final String ENTER_ID_MESSAGE = "Enter your unique client-number";
}
