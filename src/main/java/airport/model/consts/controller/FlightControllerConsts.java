package airport.model.consts.controller;

public class FlightControllerConsts {
    private FlightControllerConsts() {};
    public static final String ENTER_CITY_MESSAGE = "Please, enter city name you want to visit from available list";
    public static final String NO_AVAILABLE_FLIGHTS_MESSAGE = "There are no available flights to such city, enter another city from list for search";
    public static final String FLIGHT_CONTROL_STRING_FORMAT = "%s %s %s %23s";
    public static final String DESTINATION_WORD = "destination";
}
