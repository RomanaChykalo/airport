package airport.model.consts.controller;

public class OrderContrConsts {
    private OrderContrConsts() {};
    public static final String CANNOT_CREATE_ORDER_FOR_NO_USER = "You can't create order for not existing user!";
    public static final String REGISTRY_ORDER_GREETING_MESSAGE = "You have successfully registered your order";
}
