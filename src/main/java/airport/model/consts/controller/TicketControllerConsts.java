package airport.model.consts.controller;

public class TicketControllerConsts {
    private TicketControllerConsts() {};
    public static final String ENTER_FLIGHT_ID_MESSAGE = "Enter id of raise, which will be suitable for you, and we will show you all information";
    public static final String NO_AVAILABLE_TICKETS_ON_RAISE = "Sorry, there are no available tickets in this raise chose another one...";
    public static final String COLUMN_NAMES_PATTERN = "Ticket id  Place number  Flight id  Order id  Price";
    public static final String NULLABLE_GENERAL_COST = "General cost of your tickets is: 0";
    public static final String GENERAL_TICKET_COST_MESSAGE = "General cost of your tickets is: ";

    public static final String ENTER_TICKET_ID_MESSAGE = "Enter ticket id, you want to reserve";
    public static final String ENTER_CORRECT_DATA_MESSAGE = "Enter correct data!";
}
