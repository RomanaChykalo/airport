package airport.model.consts.dao;

public class DaoConsts {
    private DaoConsts() {
    }

    public static final String CREATE_CLIENT_SQL = "INSERT into `client` (name, surname, email, phone, id) VALUES (?, ?, ?, ?, ?)";
    public static final String FIND_CLIENT_BY_ID_EMAIL_SQL = "SELECT * FROM airport.client WHERE id=? and email=?";
    public static final String FIND_DISTINCT_DESTINATION = "SELECT distinct destination FROM flight";
    public static final String FIND_AVAILABLE_FLIGHT_BY_DESTINATION_SQL = "SELECT f.id,a.destination,a.departure_time,a.arrival_time FROM airport.flight a join flight_has_seat_place f on a.id = f.flight_id where destination=? and is_available=true order by f.id";
    public static final String DESTINATION_WORD = "destination";
    public static final String ID_COLUMN = "f.id";
    public static final String DESTINATION_COLUMN = "a.destination";
    public static final String DEPARTURE_TIME_COLUMN = "a.departure_time";
    public static final String ARRIVAL_TIME_COLUMN = "a.arrival_time";
    public static final String EMPTY_SPACES_6 = "      ";
    public static final String EMPTY_SPACES_2 = "  ";
    public static final String EMPTY_SPACES_4 = "    ";
    public static final String CREATE_ORDER_SQL = "INSERT into `order` (created_at, is_paid, client_id) VALUES ( ?, ?, ?)";
    public static final String FIND_TICKET_BY_ID_SQL = "Select * from flight_has_seat_place where flight_id=? and is_available=true";
    public static final String FIND_TICKETS_BY_CLIENT_ID_SQL ="SELECT * FROM flight_has_seat_place f JOIN `airport`.`order` o ON f.order_id = o.id and client_id = ?";
    public static final String GET_GENERAL_PRICE_USER_TICKETS_SQL= "SELECT SUM(price) sum from (flight_has_seat_place f JOIN `airport`.`order` o ON f.order_id = o.id and client_id=?)";
    public static final String UPDATE_TICKET_AFTER_ORDER_SQL="UPDATE flight_has_seat_place SET order_id=?, is_available=false where id=?";
    public static final String FIND_TICKET_BY_ID= "SELECT * FROM flight_has_seat_place WHERE id=?";


}
