package airport.model.consts.entities;

public class AircraftConsts {

    public static final String AIRCRAFT_STRING_FORMAT = "%-8s %-10s %-5s";
    public static final String ID_WORD= "id";
    public static final String MODEL_WORD="model";
    public static final String SEAT_AMOUNT_WORD="seat_amount";
}
