package airport.model.consts.entities;

public class ClientConsts {
    private ClientConsts() {
    }

    public static final String CLIENT_STRING_FORMAT = "%-20s %-10s %-2s %-10s,%-10s";
    public static final String NAME_WORD = "name";
    public static final String SURNAME_WORD = "surname";
    public static final String EMAIL_WORD = "email";
    public static final String PHONE_WORD = "phone";
    public static final String CLIENT_WORD = "client";
}
