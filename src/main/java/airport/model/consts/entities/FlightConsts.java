package airport.model.consts.entities;

public class FlightConsts {
    private FlightConsts() {
    }

    public static final String FLIGHT_WORD = "flight";
    public static final String DESTINATION_WORD = "destination";
    public static final String DEPARTURE_TIME_WORD= "departure_time";
    public static final String ARRIVAL_TIME_WORD = "arrival_time";
    public static final String AIRCRAFT_ID_WORD ="aircraft_id";
    public static final String FLIGHT_STRING_FORMAT= "%-5s %-11s %-27s %10s %5s";
}
