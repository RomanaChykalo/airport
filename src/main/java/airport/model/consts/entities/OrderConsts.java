package airport.model.consts.entities;

public class OrderConsts {
    private OrderConsts() {
    }

    public static final String ORDER_WORD = "order";
    public static final String CREATED_AT_WORD = "created_at";
    public static final String IS_PAID_WORD = "is_paid";
    public static final String CLIENT_ID_WORD = "client_id";
}
