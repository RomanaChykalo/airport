package airport.model.consts.entities;

public class TicketConsts {
private TicketConsts(){}
    public static final String SEAT_PLACE_WORD = "seat_place";
    public static final String FLIGHT_HAS_SEAT_PLACE_WORD = "flight_has_seat_place";
    public static final String SEAT_PLACE_ID_WORD ="seat_place_id";
    public static final String FLIGHT_ID_WORD ="flight_id";
    public static final String ORDER_ID_WORD ="order_id";
    public static final String PRICE_WORD ="price";
    public static final String IS_AVAILABLE_WORD ="is_available";
    public static final String TICKET_STRING_FORMAT= "%7s %10s %15s %10s %10s";
}
