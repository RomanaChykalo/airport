package airport.model.entities;

import airport.model.annotation.Column;
import airport.model.annotation.PrimaryKey;

import static airport.model.consts.entities.AircraftConsts.*;

public class Aircraft {
    @PrimaryKey
    @Column(name = ID_WORD)
    private Integer id;
    @Column(name = MODEL_WORD)
    private String model;
    @Column(name = SEAT_AMOUNT_WORD)
    private Integer seatAmount;

    public Aircraft(Integer id, String model, Integer seatAmount) {
        this.id = id;
        this.model = model;
        this.seatAmount = seatAmount;
    }

    public Aircraft() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getSeatAmount() {
        return seatAmount;
    }

    public void setSeatAmount(Integer seatAmount) {
        this.seatAmount = seatAmount;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Aircraft aircraft = (Aircraft) object;

        if (model != null ? !model.equals(aircraft.model) : aircraft.model != null) return false;
        return seatAmount != null ? seatAmount.equals(aircraft.seatAmount) : aircraft.seatAmount == null;
    }

    @Override
    public int hashCode() {
        int result = model != null ? model.hashCode() : 0;
        result = 31 * result + (seatAmount != null ? seatAmount.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(AIRCRAFT_STRING_FORMAT, id, model, seatAmount);
    }


}
