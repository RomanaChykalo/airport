package airport.model.entities;

import airport.model.annotation.Column;
import airport.model.annotation.PrimaryKey;
import airport.model.annotation.Table;

import static airport.model.consts.entities.AircraftConsts.ID_WORD;
import static airport.model.consts.entities.ClientConsts.*;

@Table(name =CLIENT_WORD)
public class Client {
    @PrimaryKey
    @Column(name = ID_WORD)
    private Integer id;
    @Column(name = NAME_WORD)
    private String name;
    @Column(name = SURNAME_WORD)
    private String surname;
    @Column(name = EMAIL_WORD)
    private String email;
    @Column(name = PHONE_WORD)
    private String phone;

    public Client(Integer id, String name, String surname, String email, String phone) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    public Client() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Client client = (Client) object;

        if (!id.equals(client.id)) return false;
        if (name != null ? !name.equals(client.name) : client.name != null) return false;
        if (surname != null ? !surname.equals(client.surname) : client.surname != null) return false;
        if (email != null ? !email.equals(client.email) : client.email != null) return false;
        return phone != null ? phone.equals(client.phone) : client.phone == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(CLIENT_STRING_FORMAT, id, name, surname, email, phone);
    }

}
