package airport.model.entities;

import airport.model.annotation.Column;
import airport.model.annotation.PrimaryKey;
import airport.model.annotation.Table;
import java.sql.Timestamp;

import static airport.model.consts.entities.AircraftConsts.ID_WORD;
import static airport.model.consts.entities.FlightConsts.*;

@Table(name =FLIGHT_WORD)
public class Flight {
    @PrimaryKey
    @Column(name = ID_WORD)
    private Integer id;
    @Column(name = DESTINATION_WORD)
    private String destination;
    @Column(name = DEPARTURE_TIME_WORD)
    private Timestamp departureTime;
    @Column(name = ARRIVAL_TIME_WORD)
    private Timestamp arrivalTime;
    @Column(name = AIRCRAFT_ID_WORD)
    private Integer aircraftId;

    public Flight(Integer id, String destination, Timestamp departureTime, Timestamp arrivalTime, Integer aircraftId) {
        this.id = id;
        this.destination = destination;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.aircraftId = aircraftId;
    }

    public Flight(String destination) {
        this.destination = destination;
    }
    public Flight() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Integer getAircraftId() {
        return aircraftId;
    }

    public void setAircraftId(Integer aircraftId) {
        this.aircraftId = aircraftId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Flight flight = (Flight) object;

        if (!id.equals(flight.id)) return false;
        if (destination != null ? !destination.equals(flight.destination) : flight.destination != null) return false;
        if (departureTime != null ? !departureTime.equals(flight.departureTime) : flight.departureTime != null)
            return false;
        if (arrivalTime != null ? !arrivalTime.equals(flight.arrivalTime) : flight.arrivalTime != null) return false;
        return aircraftId != null ? aircraftId.equals(flight.aircraftId) : flight.aircraftId == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        result = 31 * result + (departureTime != null ? departureTime.hashCode() : 0);
        result = 31 * result + (arrivalTime != null ? arrivalTime.hashCode() : 0);
        result = 31 * result + (aircraftId != null ? aircraftId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(FLIGHT_STRING_FORMAT, id, destination, departureTime, arrivalTime, aircraftId);
    }

}
