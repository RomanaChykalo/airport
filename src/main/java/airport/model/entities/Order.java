package airport.model.entities;

import airport.model.annotation.Column;
import airport.model.annotation.PrimaryKey;
import airport.model.annotation.Table;

import java.sql.Timestamp;

import static airport.model.consts.entities.AircraftConsts.AIRCRAFT_STRING_FORMAT;
import static airport.model.consts.entities.AircraftConsts.ID_WORD;
import static airport.model.consts.entities.OrderConsts.*;

@Table(name = ORDER_WORD)
public class Order {
    @PrimaryKey
    @Column(name = ID_WORD)
    private Integer id;
    @Column(name = CREATED_AT_WORD)
    private Timestamp createdAt;
    @Column(name = IS_PAID_WORD)
    private boolean isPaid;
    @Column(name = CLIENT_ID_WORD)
    private Integer clientId;

    public Order() {
    }

    public Order(Integer id, Timestamp createdAt, boolean isPaid, Integer clientId) {
        this.id = id;
        this.createdAt = createdAt;
        this.isPaid = isPaid;
        this.clientId = clientId;
    }

    public Order(Timestamp createdAt, boolean isPaid, Integer clientId) {
        this.createdAt = createdAt;
        this.isPaid = isPaid;
        this.clientId = clientId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Order order = (Order) object;

        if (isPaid != order.isPaid) return false;
        if (!id.equals(order.id)) return false;
        return createdAt != null ? createdAt.equals(order.createdAt) : order.createdAt == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (isPaid ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(AIRCRAFT_STRING_FORMAT, id, createdAt, isPaid);
    }

}
