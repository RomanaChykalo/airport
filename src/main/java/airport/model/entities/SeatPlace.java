package airport.model.entities;

import airport.model.annotation.Column;
import airport.model.annotation.PrimaryKey;
import airport.model.annotation.Table;

import static airport.model.consts.entities.AircraftConsts.ID_WORD;
import static airport.model.consts.entities.FlightConsts.AIRCRAFT_ID_WORD;
import static airport.model.consts.entities.TicketConsts.SEAT_PLACE_WORD;

@Table(name = SEAT_PLACE_WORD)
public class SeatPlace {
    @PrimaryKey
    @Column(name = ID_WORD)
    private Integer id;
    @Column(name = AIRCRAFT_ID_WORD)
    private Integer aircraftId;

    public SeatPlace(Integer id, Integer aircraftId) {
        this.id = id;
        this.aircraftId = aircraftId;
    }

    public SeatPlace() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAircraftId() {
        return aircraftId;
    }

    public void setAircraftId(Integer aircraftId) {
        this.aircraftId = aircraftId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        SeatPlace seatPlace = (SeatPlace) object;

        if (!id.equals(seatPlace.id)) return false;
        return aircraftId != null ? aircraftId.equals(seatPlace.aircraftId) : seatPlace.aircraftId == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (aircraftId != null ? aircraftId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SeatPlace{" +
                "id=" + id +
                ", aircraftId=" + aircraftId +
                '}';
    }
}
