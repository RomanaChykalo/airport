package airport.model.entities;

import airport.model.annotation.Column;
import airport.model.annotation.PrimaryKey;
import airport.model.annotation.Table;

import java.math.BigDecimal;

import static airport.model.consts.entities.AircraftConsts.ID_WORD;
import static airport.model.consts.entities.TicketConsts.*;

@Table(name =FLIGHT_HAS_SEAT_PLACE_WORD)
public class Ticket {
    @PrimaryKey
    @Column(name = ID_WORD)
    private Integer id;
    @Column(name = SEAT_PLACE_ID_WORD)
    private Integer seatPlaceId;
    @Column(name = FLIGHT_ID_WORD)
    private Integer flightId;
    @Column(name = ORDER_ID_WORD)
    private Integer orderId;
    @Column(name = PRICE_WORD)
    private BigDecimal price;
    @Column(name = IS_AVAILABLE_WORD)
    private Boolean isAvailable;

    public Ticket(Integer id, Integer seatPlaceId, Integer flightId, Integer orderId, BigDecimal price,
                  Boolean isAvailable) {
        this.id = id;
        this.seatPlaceId = seatPlaceId;
        this.flightId = flightId;
        this.orderId = orderId;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public Ticket() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSeatPlaceId() {
        return seatPlaceId;
    }

    public void setSeatPlaceId(Integer seatPlaceId) {
        this.seatPlaceId = seatPlaceId;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Ticket that = (Ticket) object;

        if (seatPlaceId != null ? !seatPlaceId.equals(that.seatPlaceId) : that.seatPlaceId != null) return false;
        if (flightId != null ? !flightId.equals(that.flightId) : that.flightId != null) return false;
        if (orderId != null ? !orderId.equals(that.orderId) : that.orderId != null) return false;
        return price != null ? price.equals(that.price) : that.price == null;
    }

    @Override
    public int hashCode() {
        int result = seatPlaceId != null ? seatPlaceId.hashCode() : 0;
        result = 31 * result + (flightId != null ? flightId.hashCode() : 0);
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(TICKET_STRING_FORMAT, id, seatPlaceId, flightId, orderId, price);
    }
}
