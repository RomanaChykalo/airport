package airport.persistant;

import airport.model.annotation.Column;
import airport.model.annotation.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.*;

public class Transformer<T> {
    private final Class<T> clazz;
    private static Logger logger = LogManager.getLogger(Transformer.class);

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet resultSet) throws SQLException {
        Object model = null;
        try {
            model = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        if (fieldType == String.class) {
                            field.set(model, resultSet.getString(name));
                        } else if (fieldType == Integer.class) {
                            field.set(model, resultSet.getInt(name));
                        } else if (fieldType == Timestamp.class) {
                            field.set(model, resultSet.getTimestamp(name));
                        } else if (fieldType == BigDecimal.class) {
                            field.set(model, resultSet.getBigDecimal(name));
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            logger.info(e.getMessage());
        } catch (InstantiationException e) {
            logger.info(e.getMessage());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.info(e.getMessage());
        }
        return model;
    }
}
