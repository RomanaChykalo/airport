package airport.service;

import airport.model.entities.Client;

import java.sql.SQLException;

public interface ClientService {
    int create(Client entity) throws SQLException;

    Client findByIdAndEmail(Integer id, String email) throws SQLException;
}
