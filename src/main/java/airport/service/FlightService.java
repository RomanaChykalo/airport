package airport.service;

import java.sql.SQLException;
import java.util.List;

public interface FlightService {
    List<String> findAllDestinations() throws SQLException;

    List<String> findByDestination(String destination);
}
