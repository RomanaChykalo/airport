package airport.service;

import java.sql.SQLException;

public interface OrderService {
    Integer create(Integer clientId) throws SQLException;
}
