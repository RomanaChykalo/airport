package airport.service;

import airport.model.entities.Ticket;

import java.sql.SQLException;
import java.util.List;

public interface TicketService {
    List<Ticket> findByFlightId(Integer id) throws SQLException;
    void update(Integer orderId, Integer seatPlaceId) throws SQLException;
    Ticket findById(Integer id) throws SQLException;
    List<Ticket> findByClientId(Integer clientId) throws SQLException;
    Integer getGeneralPrice(Integer clientId) throws SQLException;
}
