package airport.service.implementation;

import airport.dao.ClientDao;
import airport.dao.implementation.ClientDaoImpl;
import airport.model.entities.Client;
import airport.service.ClientService;

import java.sql.SQLException;

public class ClientServiceImpl implements ClientService {
    private static ClientDao clientDao = new ClientDaoImpl();

    public int create(Client entity) throws SQLException {
        return clientDao.create(entity);
    }
    public Client findByIdAndEmail(Integer id, String email)throws SQLException {
        Client client = clientDao.findByIdAndEmail(id,email);
        return client;
    }
}
