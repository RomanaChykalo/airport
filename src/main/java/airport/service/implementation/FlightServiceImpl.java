package airport.service.implementation;

import airport.dao.FlightDao;
import airport.dao.implementation.FlightDaoImpl;
import airport.service.FlightService;

import java.sql.SQLException;
import java.util.List;

public class FlightServiceImpl implements FlightService {
    private static FlightDao flightDao = new FlightDaoImpl();

    public List<String> findAllDestinations() throws SQLException {
        return flightDao.findAllDestinations();
    }

    public List<String> findByDestination(String destination){
        return flightDao.findByDestination(destination);
    }
}
