package airport.service.implementation;

import airport.dao.OrderDao;
import airport.dao.implementation.OrderDaoImpl;
import airport.service.OrderService;

import java.sql.SQLException;

public class OrderServiceImpl implements OrderService {
    private static OrderDao orderDao = new OrderDaoImpl();

    public Integer create(Integer clientId) throws SQLException {
        return orderDao.create(clientId);
    }
   /* public Integer findOrderIdByCreatedAt(Timestamp time)throws SQLException {
        Integer orderId = orderDao.getOrderIdByTimeOfCreation(time);
        return orderId;
    }*/
}
