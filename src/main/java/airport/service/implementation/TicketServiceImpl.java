package airport.service.implementation;

import airport.dao.TicketDao;
import airport.dao.implementation.TicketDaoImpl;
import airport.model.entities.Ticket;
import airport.service.TicketService;

import java.sql.SQLException;
import java.util.List;

public class TicketServiceImpl implements TicketService {
    private static TicketDao ticketDao = new TicketDaoImpl();

    public List<Ticket> findByFlightId(Integer id) throws SQLException {
        return ticketDao.findByFlightId(id);
    }

    /*public List<Ticket> findAvailableTicketsByFlightId(Integer id) throws SQLException {
        return ticketDao.findByFlightId(id);
    }*/


    public void update(Integer orderId, Integer seatPlaceId) throws SQLException {
        ticketDao.update(orderId,seatPlaceId);
    }

    public Ticket findById(Integer id) throws SQLException {
        return ticketDao.findById(id);
    }
    public List<Ticket> findByClientId(Integer clientId) throws SQLException {
        return ticketDao.findByClientId(clientId);
    }
    public Integer getGeneralPrice(Integer clientId) throws SQLException {
        return ticketDao.getGeneralPrice(clientId);
    }
}
