package airport.view;

import airport.controller.*;
import airport.model.entities.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static airport.controller.FlightController.*;
import static airport.controller.TicketController.*;
import static airport.model.consts.GeneralViewConsts.*;
import static airport.model.consts.controller.TicketControllerConsts.ENTER_CORRECT_DATA_MESSAGE;


public class ProgramFlow {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(ProgramFlow.class);

    private Integer authenticateUser() {
        logger.info(LOGIN_OR_REGISTER_MESSAGE);
        String line = input.nextLine();
        if (line.equalsIgnoreCase("r")) {
            return ClientController.createClientAndGetId();
        } else if (line.equalsIgnoreCase("l")) {
            return ClientController.findClientByIdAndEmail().getId();
        } else {
            logger.info(ENTER_CORRECT_DATA_MESSAGE);
            return authenticateUser();
        }
    }

    private boolean endLoop() {
        logger.info(TYPE_YES_OR_NO_MESSAGE);
        input.nextLine();
        String choice = input.nextLine();
        if (choice.equalsIgnoreCase(YES_WORD)) {
            return false;
        } else if (choice.equalsIgnoreCase(NO_WORD)) {
            System.exit(0);
        }
        return true;
    }

    public void createOrder(Integer userId) {
        Integer orderId = 0;
        boolean isEnd = false;
        do {
            printAllDestination();
            printFlightsByOneCity();
            printTicketsSortedByFlightId();
            Ticket seatPlaceInTicket = findTicketById();
            if (orderId == 0) {
                orderId = OrderController.createOrderAndReturnId(userId);
            }
            updateTicket(orderId, seatPlaceInTicket.getSeatPlaceId());
            isEnd = endLoop();
        } while (!isEnd);
    }

    public void loop(Integer userId) {
        logger.info(CREATE_ORDER_MESSAGE);
        logger.info(SHOW_ORDER_MESSAGE);
        logger.info(QUIT_MESSAGE);

        Integer choice = input.nextInt();
        switch (choice) {
            case 1:
                createOrder(userId);
                printUserTickets(userId);
                printGeneralPrice(userId);
                break;
            case 2:
                printUserTickets(userId);
                printGeneralPrice(userId);
                break;
            default:
                logger.info(YOU_EXIT_MESSAGE);
                System.exit(0);
        }
        loop(userId);
    }

    public void programProcess() {
        logger.info(WELCOME_MESSAGE);
        Integer userId = authenticateUser();
        loop(userId);
    }
}
