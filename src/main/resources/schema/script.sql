-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZEVISIBLE ,RO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema airport
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema airport
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `airport` DEFAULT CHARACTER SET utf8 ;
USE `airport` ;

-- -----------------------------------------------------
-- Table `airport`.`aircraft`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`aircraft` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `model` VARCHAR(45) NULL,
  `seat_ amount` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `airport`.`flight`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`flight` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `destination` VARCHAR(45) NULL,
  `departure_time` DATETIME NULL,
  `arrival_time` DATETIME NULL,
  `aircraft_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_flight_aircraft_idx` (`aircraft_id` ASC),
  CONSTRAINT `fk_flight_aircraft`
    FOREIGN KEY (`aircraft_id`)
    REFERENCES `airport`.`aircraft` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `airport`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `phone` VARCHAR(12) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `airport`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NULL,
  `is_paid` BIT(1) NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_client1_idx` (`client_id` ASC),
  CONSTRAINT `fk_order_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `airport`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `airport`.`seat_place`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`seat_place` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `aircraft_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_seat_place_aircraft1_idx` (`aircraft_id` ASC),
  CONSTRAINT `fk_seat_place_aircraft1`
    FOREIGN KEY (`aircraft_id`)
    REFERENCES `airport`.`aircraft` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `airport`.`flight_has_seat_place`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`flight_has_seat_place` (
  `seat_place_id` INT NOT NULL,
  `flight_id` INT NOT NULL,
  `price` DECIMAL(10) NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_available` BIT(1) NULL,
  `order_id` INT NOT NULL,
  INDEX `fk_seat_place_has_flight_flight1_idx` (`flight_id` ASC),
  INDEX `fk_seat_place_has_flight_seat_place1_idx` (`seat_place_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_flight_has_seat_place_order1_idx` (`order_id` ASC),
  CONSTRAINT `fk_seat_place_has_flight_seat_place1`
    FOREIGN KEY (`seat_place_id`)
    REFERENCES `airport`.`seat_place` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_seat_place_has_flight_flight1`
    FOREIGN KEY (`flight_id`)
    REFERENCES `airport`.`flight` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_flight_has_seat_place_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `airport`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

ALTER TABLE flight_has_seat_place MODIFY order_id INT NULL

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `airport`.`aircraft`
-- -----------------------------------------------------
START TRANSACTION;
USE `airport`;
INSERT INTO `airport`.`aircraft` (`id`, `model`, `seat_ amount`) VALUES (1, 'Bismark', 10);
INSERT INTO `airport`.`aircraft` (`id`, `model`, `seat_ amount`) VALUES (2, 'Kongo', 20);
INSERT INTO `airport`.`aircraft` (`id`, `model`, `seat_ amount`) VALUES (3, 'Yamato', 15);
INSERT INTO `airport`.`aircraft` (`id`, `model`, `seat_ amount`) VALUES (4, 'Renown', 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `airport`.`flight`
-- -----------------------------------------------------
START TRANSACTION;
USE `airport`;
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (1, 'London', '2019-07-18 09:17:17', '2019-07-18 13:17:17', 1);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (2, 'Tokio', '2019-06-18 10:00:00', '2019-06-18 13:00:00', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (3, 'Berlin', '2019-05-20 08:00:00', '2019-05-20 18:00:00', 3);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (4, 'Paris', '2019-04-01 18:00:00', '2019-04-01 21:00:00', 4);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (5, 'London', '2019-06-10 16:00:00', '2019-06-10 18:00:00', 1);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (6, 'Gdansk', '2019-06-10 16:00:00', '2019-06-10 17:30:00', 3);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (7, 'London', '2019-06-18 13:00:00', '2019-06-18 18:00:00', 3);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (8, 'Tokio', '2019-06-18 08:00:00', '2019-07-18 09:17:17', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (9, 'Berlin', '2019-05-20 18:00:00', '2019-05-20 20:00:00', 3);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (10, 'Paris', '2019-05-20 08:00:00', '2019-05-20 13:17:17', 1);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (11, 'London', '2019-06-10 20:00:00', '2019-06-10 22:00:00', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (12, 'Paris', '2019-06-18 08:00:00', '2019-06-18 12:00:00', 1);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (13, 'Berlin', '2019-06-10 16:00:00', '2019-06-10 18:00:00', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (14, 'London', '2019-07-18 09:17:17', '2019-07-18 12:00:00', 1);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (15, 'Berlin', '2019-06-10 20:00:00', '2019-06-10 21:00:00', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (16, 'London', '2019-05-01 18:00:00', '2019-05-01 20:00:00', 1);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (17, 'London', '2019-05-02 12:00:00', '2019-05-02 15:00:00', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (18, 'Berlin', '2019-05-01 20:00:00', '2019-05-01 21:00:00', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (19, 'Paris', '2019-05-01 18:00:00', '2019-05-01 21:00:00', 3);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (20, 'Paris', '2019-05-02 12:00:00', '2019-05-02 15:00:00', 1);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (21, 'Paris', '2019-07-18 09:17:17', '2019-07-18 12:00:00', 2);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (22, 'London', '2019-05-08 12:00:00', '2019-05-08 15:00:00', 3);
INSERT INTO `airport`.`flight` (`id`, `destination`, `departure_time`, `arrival_time`, `aircraft_id`) VALUES (23, 'Paris', '2019-05-08 12:00:00', '2019-05-08 15:00:00', 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `airport`.`seat_place`
-- -----------------------------------------------------
START TRANSACTION;
USE `airport`;
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (1, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (2, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (3, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (4, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (5, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (6, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (7, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (8, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (9, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (10, 1);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (11, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (12, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (13, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (14, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (15, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (16, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (17, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (18, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (19, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (20, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (21, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (22, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (23, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (24, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (25, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (26, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (27, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (28, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (29, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (30, 2);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (31, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (32, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (33, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (34, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (35, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (36, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (37, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (38, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (39, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (40, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (41, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (42, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (43, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (44, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (45, 3);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (46, 4);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (47, 4);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (48, 4);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (49, 4);
INSERT INTO `airport`.`seat_place` (`id`, `aircraft_id`) VALUES (50, 4);

COMMIT;

START TRANSACTION;
USE `airport`;
INSERT INTO `airport`.`client` (`id`, `name`,`surname`,`email`,`phone`) VALUES (1, 'Olga','Patko','olga@ukr.net','380937597167');
INSERT INTO `airport`.`client` (`id`, `name`,`surname`,`email`,`phone`) VALUES (2, 'Petro','Melnyk','petro@ukr.net','38093000000');
INSERT INTO `airport`.`client` (`id`, `name`,`surname`,`email`,`phone`) VALUES (3, 'Max','Chykalo','max@ukr.net','38093011100');
COMMIT;














START TRANSACTION;
USE `airport`;
INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (1, '2019-04-10 16:00:00',true ,1);
INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (2, '2019-01-10 16:00:00',false ,2);

INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (3, '2019-02-10 16:00:00',true ,1);

INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (4, '2019-02-10 16:00:00',true ,3);

INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (5, '2019-03-18 16:00:00',true ,1);

INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (6, '2019-05-10 16:00:00',true ,2);

INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (7, '2019-06-10 16:00:00',false ,3);

INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (8, '2019-05-02 16:00:00',true ,1);

INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (9, '2019-04-01 16:00:00',true ,2);
INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (10, '2019-04-10 16:00:00',false ,3);
INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (11, '2019-05-10 16:00:00',true ,3);
INSERT INTO `airport`.`order` (`id`, `created_at`,`is_paid`,`client_id`)
  VALUES (12, '2019-03-10 16:00:00',false ,2);
 COMMIT;

START TRANSACTION;
USE `airport`;
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (1, 1,500,1,true,1);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (2, 2, 230,2,true,1);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (3, 3,280,3,false,2 );
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (4, 4,150,4,true,2);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (5, 5, 240,5,true ,1);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (6, 6, 600,6,true,3);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (7, 7, 550,7,false,4);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (8, 8, 440,8,true ,4);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (9, 9, 250,9,true ,1);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (10, 10, 340,10,true ,2);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (11, 11,280,11,true ,3);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (12, 12, 420,12,true ,4);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (13, 13,230,13,false ,5);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (14, 14,550,14,true ,2);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (15, 15,350,15,false, 6);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (16, 16,600,16,true ,3);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (17, 17, 340,17,false ,1);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (18, 18,500,18, true ,6);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (19, 19,430,19,true ,4);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (20, 20,560,20,false ,5);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (21, 21,210,21,true ,4);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (22, 22,340,22,true,1);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (23, 23,280,23,false ,11);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (24, 1,150,24,true ,10);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (25, 2,220,25,true ,8);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (26, 3,550,26,false ,9);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (27, 4,450,27,true,11);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (28, 5,130,28,false ,10);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (29, 6,600,29,true ,9);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (30, 7,280,30,true ,10);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (31, 8,600,31,true ,12);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (32, 9,150,32,false ,11);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (33, 10,420,33,true ,6);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (34, 11,280,34,false ,7);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (35, 12,320,35,true ,7);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (36, 13,300,36,true ,8);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (37, 14,350,37,true ,7);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (38, 15,420,38,false ,8);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (39, 16,450,39,true ,9);
INSERT INTO `airport`.`flight_has_seat_place` (`seat_place_id`, `flight_id`,`price`,`id`,`is_available`,`order_id`)
  VALUES (40, 17,600,40,true ,10);
  UPDATE flight_has_seat_place SET order_id = Null;
COMMIT;
use airport;
SELECT f.id,f.price FROM flight_has_seat_place f JOIN `airport`.`order` o ON f.order_id = o.id and client_id = 3;


SELECT f.id,a.destination,a.departure_time,a.arrival_time FROM airport.flight a join flight_has_seat_place f on a.id = f.flight_id where destination='london' and is_available=true order by f.id;


UPDATE flight_has_seat_place SET is_available = true  WHERE id=13;

UPDATE flight_has_seat_place SET order_id=1, is_available=false where id=22;
SELECT f.id,f.price FROM flight_has_seat_place f JOIN `airport`.`order` o ON f.order_id = o.id and client_id = 3;
Select * from flight_has_seat_place where flight_id=4 and is_available=true;

SELECT SUM(Price) suma from (flight_has_seat_place f JOIN `airport`.`order` o ON f.order_id = o.id and client_id = 1);

SELECT f.id,a.destination,a.departure_time,a.arrival_time FROM airport.flight a join flight_has_seat_place f on a.id = f.flight_id where destination='London' and is_available=true order by f.id;
use airport;
