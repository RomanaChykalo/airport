package airport.controller;

import airport.model.entities.Client;
import airport.service.ClientService;

import airport.service.implementation.ClientServiceImpl;
import org.junit.Test;
/*import org.junit.jupiter.api.Test;*/
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientControllerTest {
    ClientService clientService = new ClientServiceImpl();


    @Test
    public void findClientByIdAndEmail() {
        try {
            Client foundClient = clientService.findByIdAndEmail(1, "olga@ukr.net");
            Client client = new Client(1, "Olga", "Patko", "olga@ukr.net", "380937597167");
            assertEquals(client, foundClient);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Test
    public void testNoFindClient() {
        try {
            Client foundClient = clientService.findByIdAndEmail(-1,"foo");
            assertNull(foundClient);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() {
        List<String> mockList = mock(List.class);
        mockList.add("First");
        when(mockList.get(0)).thenReturn("Mockito");
        when(mockList.get(1)).thenReturn("JCG");
        assertEquals("Mockito", mockList.get(0));
        assertEquals("JCG", mockList.get(1));
    }
}