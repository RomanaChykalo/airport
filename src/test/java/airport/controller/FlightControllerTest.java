package airport.controller;

import airport.service.FlightService;
import airport.service.implementation.FlightServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class FlightControllerTest {
    private String city;

    public FlightControllerTest(String p1) {
        city = p1;
    }

    @Parameterized.Parameters
    public static Collection data() {
        List<String> cities = new ArrayList<>();
        cities.add("Odessa");
        cities.add("Lviv");
        cities.add("Ternopil");
        return cities;
    }

    FlightService flightService = new FlightServiceImpl();

    @Test
    public void printAllDestination() {
        try {
            assertTrue(flightService.findAllDestinations().stream().anyMatch(a -> a.contains("London")));
        } catch (SQLException e) {
            e.getCause();
        }
    }

    @Test
    public void printFlightsByOneCity() {
        Assertions.assertEquals(flightService.findByDestination(city).size(), 0);
    }
}