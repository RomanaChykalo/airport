package airport.controller;

import airport.exceptions.DuplicateEntityException;
import airport.service.OrderService;
import airport.service.implementation.OrderServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class OrderControllerTest {

    OrderService orderService = new OrderServiceImpl();

    @Test
   public void createOrderAndReturnId() {
        DuplicateEntityException e = assertThrows(DuplicateEntityException.class,
                () -> {
                    throw new DuplicateEntityException(" Entity was not created, entity with such id already" +
                            " exists, please, try again, enter correct data\n");
                });
        try {
            assertEquals(orderService.create(9), e.getMessage());
        } catch (SQLException ex) {
            ex.getMessage();
        }
    }

}