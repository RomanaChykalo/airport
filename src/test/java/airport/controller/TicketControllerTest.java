package airport.controller;

import airport.model.entities.Ticket;
import airport.service.TicketService;
import airport.service.implementation.TicketServiceImpl;
import org.junit.Test;
/*import org.junit.jupiter.api.Test;*/

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TicketControllerTest {

    TicketService ticketService = new TicketServiceImpl();

    @Test
    public void printTicketsSortedByFlightId() {
        try {
            List<Ticket> ticketList = ticketService.findByFlightId(55);
            assertEquals(ticketList.size(), 0);
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Test
    public void printIsReallyTicketsSortedByFlightId() {
        try {
            List<Ticket> ticketList = ticketService.findByFlightId(11);
            Integer ticketFirstId = ticketList.get(0).getId();
            Integer ticketSecondId = ticketList.get(1).getId();
            boolean isSecondIdBiggerThenFirst = ticketSecondId > ticketFirstId;
            assertTrue(isSecondIdBiggerThenFirst);
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Test
    public void printIsReallyPrintOnlyAvailableTickets() {
        try {
            List<Ticket> ticketList = ticketService.findByFlightId(10);
            assertEquals(ticketList.size(), 0);
        } catch (SQLException e) {
            e.getMessage();
        }
    }

    @Test
    public void updateTicket() {
        try {
            ticketService.update(1,1);
            Boolean availability = ticketService.findById(1).getAvailable();
            assertNull(availability);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void printUserTickets() {
        try {
            List<Ticket> ticketList = ticketService.findByClientId(0);
            assertEquals(ticketList.size(), 0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void printGeneralPrice() {
        try {
            Integer cost = ticketService.getGeneralPrice(4);
            assertNotNull(cost);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void findTicketById() {
        try {
            Ticket ticket = ticketService.findById(1);
            BigDecimal price = ticket.getPrice();
            assertNotNull(price);
            assertEquals(new BigDecimal(500),price);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}